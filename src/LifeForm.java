//The parent class of any living being in the game

public abstract class LifeForm {
	String name = "default";
	double hp = 10;
	double maxhp = 10;
	int atk = 1;
	int def = 0;
	
	
	//constructor for Lifeforms
	public LifeForm (String en, double health, double maxhp, int attack, int defence){
		name = en;
		hp = health;
		maxhp = maxhp;
		atk = attack;
		def = defence;
		maxhp = health;
	
	}
	
	
	//damages the LifeForm relative to defense, then returns damage done
	public double damage(int damage){
		double tempDamage;

		//if enemy has 0 defense, double damage
		if(def == 0){
			tempDamage = damage*2;
		}else{
			tempDamage = damage *((40/def)) +3;	
		}
		
		//subtract health
		hp -= tempDamage;
		
		//System.out.println(name + " takes " + tempDamage + " damage.");
		
		//returns the damage
		return tempDamage;
	}
	
	//the ammount of damage done on an attack
	public void getDamage(){
		
	}
}
